package Laborator4;

public class Production {
    private String psp;
    private String pdp;

    public Production() {
    }

    public Production(String psp, String pdp) {
        this.psp = psp;
        this.pdp = pdp;
    }

    public String getPsp() {
        return psp;
    }

    public void setPsp(String psp) {
        this.psp = psp;
    }

    public String getPdp() {
        return pdp;
    }

    public void setPdp(String pdp) {
        this.pdp = pdp;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Production that = (Production) o;

        if (psp != null ? !psp.equals(that.psp) : that.psp != null) return false;
        return pdp != null ? pdp.equals(that.pdp) : that.pdp == null;
    }

    @Override
    public int hashCode() {
        int result = psp != null ? psp.hashCode() : 0;
        result = 31 * result + (pdp != null ? pdp.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return psp+pdp;
    }
}
