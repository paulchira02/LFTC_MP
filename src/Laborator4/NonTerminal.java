package Laborator4;

public class NonTerminal {
    private String value;

    public NonTerminal() {
    }

    public NonTerminal(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
