package Laborator4;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class LR0 {

    private Grammar grammar;
    private Map<String, List<Production>> canonic;
    private Map<String, List<Production>> goto_map;
    private Map<String, String> gotoTable;
    private Map<String, String> actionsTable;
    private List<String> display_canonic = new ArrayList<>();
    private int states;
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_RESET = "\u001B[0m";

    public LR0(Grammar grammar) {
        this.grammar = grammar;
        this.canonic = new TreeMap<>();
        this.goto_map = new TreeMap<>();
        this.gotoTable = new TreeMap<>();
        this.actionsTable = new TreeMap<>();
        this.states = 1;
    }

    /**
     * @param state  - lista initiala de stari
     * @param symbol - terminal / neterimnal
     * @return parcurg lista starilor si verific daca am punct in fata unui neterminal/terminal, daca am inversez si adaug intr-o lista.
     */
    public List<Production> gotof(List<Production> state, String symbol) {
        List<Production> finalAnalysisElements = new ArrayList<>();

        for (Production analysis_element : state) {
            if (analysis_element.getPdp().contains("." + symbol)) {
                String pdp_new = analysis_element.getPdp().replace("." + symbol, symbol + ".");
                Production element = new Production();
                element.setPsp(analysis_element.getPsp());
                element.setPdp(pdp_new);
                finalAnalysisElements.add(element);
            }
        }

        return finalAnalysisElements;
    }

    /**
     * @param analysisElements - lista initiala cu elementele de analizat
     * @return ex apel : clousue(S -> .aA)
     */
    public List<Production> closure(List<Production> analysisElements) {
        //copie in care vom memora elementele de analiza, initial populat cu lista initiala
        List<Production> finalAnalysisElements = new ArrayList<>(analysisElements);
        //copie necesara pentru instr. do while
        List<Production> copyOfAnalysisElements = new ArrayList<>(analysisElements);
        //la o noua iterare do-while, stergem productiile din copyOfAnalysisElements deorece la urmat. iterare se vor duplica elementele
        List<Production> removedElementsFrom_copyOfAnalysisElements = new ArrayList<>();

        List<String> checked_nonterminals = new ArrayList<>();
        do {
            //pt. fiecare productie din copyOfAnalysisElements
            for (Production analysis_element : copyOfAnalysisElements) {
                //daca avem o productie de forma .Ab, extragem nonterminalul
                NonTerminal nonTerminal = getNonTerminalBeforeDot(analysis_element.getPdp());
                if (nonTerminal != null && !checked_nonterminals.contains(nonTerminal.getValue())) {
                    checked_nonterminals.add(nonTerminal.getValue());
                    //pt fiecare productii ale lui A (sa zicem ca A e nonterminal), le adaugam cu punct la inceput in lista finala
                    List<Production> productionsOf = new ArrayList<>(grammar.getProdOfNonTerminal(nonTerminal));
                    for (Production production : productionsOf) {
                        if (!finalAnalysisElements.contains(production)) {
                            finalAnalysisElements.add(new Production(production.getPsp(), "." + production.getPdp()));
                        }
                    }
                }
            }
            //sterg si adaug(in removedElementsFrom_copyOfAnalysisElements) elementele comune din finalAnalysisElements si copyOfAnalysisElements
            //e un calcul ca sa nu adaugam duplicate
            for (Production production : finalAnalysisElements) {
                if (copyOfAnalysisElements.contains(production)) {
                    copyOfAnalysisElements.remove(production);
                    removedElementsFrom_copyOfAnalysisElements.add(production);
                } else {
                    if (!removedElementsFrom_copyOfAnalysisElements.contains(production)) {
                        copyOfAnalysisElements.add(production);
                    }
                }
            }
        } while (checkIfAnalysisContainsDotBeforeNonTerminal(copyOfAnalysisElements));

        return finalAnalysisElements;
    }

    /**
     * verifica daca in lista rezultata de la CLOUSURE exista . inainte de nonterminal
     *
     * @param analysisElements
     * @return
     */
    public boolean checkIfAnalysisContainsDotBeforeNonTerminal(List<Production> analysisElements) {
        for (Production analysisElement : analysisElements) {
            if (getNonTerminalBeforeDot(analysisElement.getPdp()) != null) {
                return true;
            }
        }
        return false;
    }

    /**
     * returneaza neterminalul dupa punct in caz ca exista, null altfel
     *
     * @param pdp - partea dreapata a productiei ex: S -> aB  => pdp = aB
     * @return pdp = .Ab  => returneaza A
     */
    public NonTerminal getNonTerminalBeforeDot(String pdp) {
        NonTerminal nonTerminal = new NonTerminal("vid");

        //imi declar un pattern, astfel daca gasesc un nonterminal inainte de punct, fac replace la nonterminal cu @@@
        //pdp = .aA
        //dupa replace .@@@A => in final verific daca pattern contine .@@@ => inseamana ca am gasit pct inainte de nonterminal
        String pattern = pdp;
        for (NonTerminal nt : grammar.getN()) {
            if (pattern.contains(nt.getValue())) {
                if (!pattern.contains(".@@@")) {
                    nonTerminal.setValue(nt.getValue());
                }
                pattern = pattern.replace(nt.getValue(), "@@@");
            }
        }
        if (pattern.contains(".@@@")) {
            return nonTerminal;
        }
        return null;
    }

    /**
     * @return union between nonterminals and terminals as strings
     */
    public List<String> getAllSymbols() {
        List<String> symbols = new ArrayList<>();

        for (NonTerminal nonTerminal : grammar.getN()) {
            symbols.add(nonTerminal.getValue());
        }

        for (Terminal terminal : grammar.getSigma()) {
            symbols.add(terminal.getValue());
        }

        return symbols;
    }

    /**
     * verifica daca un map contine o lista
     * verific egalitatea a 2 string-uri -> deoarece in algoritm creez productii noi si se pierde referinta la obiect
     *
     * @param map
     * @param list
     * @return
     */
    public boolean mapContainsList(Map<String, List<Production>> map, List<Production> list) {
        for (Map.Entry<String, List<Production>> entry : map.entrySet()) {
            if (entry.getValue().toString().equals(list.toString())) {
                return true;
            }
        }
        return false;
    }

    public String mapContainsListGetKey(Map<String, List<Production>> map, List<Production> list) {
        for (Map.Entry<String, List<Production>> entry : map.entrySet()) {
            if (entry.getValue().toString().equals(list.toString())) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * returneaza goto dintre o stare si un simbol
     *
     * @param state
     * @param simbol
     * @return
     */
    public String gotoBetween(String state, String simbol) {
        for (Map.Entry<String, List<Production>> entry : goto_map.entrySet()) {
            String[] params = entry.getKey().split(":");
            if (state.equals(params[1]) && simbol.equals(params[2])) {
                return params[0];
            }
        }
        return null;
    }

    public String displayProduction(List<Production> list) {
        String c = "[ ";
        for (int i = 0; i < list.size(); i++) {
            c = c + list.get(i).getPsp() + " -> " + list.get(i).getPdp();
            if (i + 1 < list.size()) {
                c = c + ", ";
            }
        }
        c = c + " ]";
        return c;
    }

    /**
     * @return memoreaza in canonic toate starile + productiile respectiv toate perechile goto
     */
    public void colCan_lr0() {

        Map<String, List<Production>> removedCanonicStates = new TreeMap<>();
        List<String> symbols = getAllSymbols();

        List<Production> analysisElements = Arrays.asList(new Production("S'", ".S"));
        List<Production> s0 = closure(analysisElements);
        canonic.put("s0", s0);
        display_canonic.add("s0= " + ANSI_YELLOW + "closure" + ANSI_RESET + "({[S -> .S]} = {" + displayProduction(closure(analysisElements))+"}");
        Map<String, List<Production>> copyOfCanonic = new TreeMap<>(canonic);

        do {
            for (Map.Entry<String, List<Production>> entry : copyOfCanonic.entrySet()) {
                for (String X : symbols) {
                    //verific daca exista o stare care are acelasi rezultat cu o alta stare
                    //ex: s2 = goto(s4,a) si goto(s4,b) este egat cu goto(s4,a) atunci ii asignam s2
                    if (gotof(entry.getValue(), X).size() != 0 && canonic.containsValue(closure(gotof(entry.getValue(), X)))) {
                        String state = mapContainsListGetKey(canonic, closure(gotof(entry.getValue(), X)));
                        goto_map.put(state + ":" + entry.getKey() + ":" + X, gotof(entry.getValue(), X));
                    }
                    if (gotof(entry.getValue(), X).size() != 0 && !canonic.containsValue(closure(gotof(entry.getValue(), X)))) {
                        canonic.put("s" + states, closure(gotof(entry.getValue(), X)));
                        display_canonic.add("s" + states + "= " + ANSI_YELLOW + "goto" + ANSI_RESET + "({" + displayProduction(entry.getValue()) + "," + X + "}) = " + ANSI_YELLOW + "closure" + ANSI_RESET + "({" + displayProduction(gotof(entry.getValue(), X)) + "}) = {" + displayProduction(closure(gotof(entry.getValue(), X))) + "})");
                        goto_map.put("s" + states + ":" + entry.getKey() + ":" + X, gotof(entry.getValue(), X));
                        states = states + 1;
                    }

                }
            }

            for (Map.Entry<String, List<Production>> entry : canonic.entrySet()) {
                if (copyOfCanonic.containsValue(entry.getValue())) {
                    copyOfCanonic.remove(entry.getKey());
                    removedCanonicStates.put(entry.getKey(), entry.getValue());
                } else {
                    if (!removedCanonicStates.containsValue(entry.getValue())) {
                        copyOfCanonic.put(entry.getKey(), entry.getValue());
                    }
                }
            }
        } while (copyOfCanonic.size() > 0);
    }

    /**
     * table - memoram goto din table sub forma unui map: s1 = goto(s0,'S')  => in map avem: cheia:=s0:S valoarea:=s1
     * actions
     */
    public void table() {
        //goto
        for (int i = 0; i < states; i++) {
            for (String simbol : getAllSymbols()) {
                String s = "s" + i;
                gotoTable.put(s + ":" + simbol, gotoBetween(s, simbol) != null ? gotoBetween(s, simbol) : "error");
            }
        }
        //actions
        boolean acc = false;
        for (int i = 0; i < states; i++) {
            List<Production> check = canonic.get("s" + i);
            int end_dot = 0;
            int not_end_dot = 0;
            for (Production production : check) {
                if (acc && (production.getPdp().endsWith(".") || !production.getPdp().endsWith(".") && production.getPdp().contains("."))) {
                    actionsTable.put("s" + i, "acceptat + conflict");
                    break;
                }
                if (production.getPdp().equals("S.")) {
                    actionsTable.put("s" + i, "acceptat");
                    acc = true;
                    continue;
                }
                if (production.getPdp().endsWith(".")) {
                    end_dot++;
                }
                if (!production.getPdp().endsWith(".") && production.getPdp().contains(".")) {
                    not_end_dot++;
                }
            }
            if (acc) {
                acc = false;
                continue;
            }
            if (end_dot == check.size()) {
                String prod = canonic.get("s" + i).get(0).getPdp().replaceAll("\\.", "");
                actionsTable.put("s" + i, "reducere " + getProductionNumberForReduce(prod));
            } else if (not_end_dot == check.size()) {
                actionsTable.put("s" + i, "shift");
            } else {
                actionsTable.put("s" + i, "conflict");
            }
        }
    }

    /**
     * @return pozitia unui element de analiza din lista de productii -> pt recucere
     */
    public int getProductionNumberForReduce(String pdp) {
        for (int i = 1; i < grammar.getP().size(); i++) {
            if (grammar.getP().get(i).getPdp().equals(pdp)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * @return pdp a unei productii dupa numarul intreg -> pt recucere
     */
    public Production getProductionForReduce(int nr_production) {
        for (int i = 1; i < grammar.getP().size(); i++) {
            if (i == nr_production) {
                return grammar.getP().get(i);
            }
        }
        return null;
    }

    public void analyze(String sequence) throws Exception {
        Stack<String> work_stack = new Stack<>();
        Stack<String> in_stack = new Stack<>();
        //banda iesire
        List<Integer> bi = new ArrayList<>();
        List<String> states = new ArrayList<>();
        work_stack.push("$");
        work_stack.push("s0");

        in_stack.push("$");
        //initializare stiva intrare
        for (String symbol : parseInput(sequence)) {
            in_stack.push(symbol);
        }

        boolean error = false;
        while (error == false) {
            try {
                String action = actionsTable.get(work_stack.peek());
                if (action.equals("conflict") || action.equals("acceptat + conflict")) {
                    System.out.println("ERROR: goto(" + work_stack.peek() + "," + in_stack.peek() + ") = conflict");
                    return;
                }
                if (action.equals("shift")) {
                    String state = gotoTable.get(work_stack.peek() + ":" + in_stack.peek());
                    states.add(state);
                    if (state.equals("error")) {
                        System.out.println("ERROR: goto(" + work_stack.peek() + "," + in_stack.peek() + ") = error");
                        return;
                    }
                    work_stack.push(in_stack.pop());
                    work_stack.push(state);
                    System.out.println(work_stack);
                    System.out.println(in_stack);
                    System.out.println("shift");
                    System.out.println();
                    continue;
                }
                if (action.contains("reducere")) {
                    int nr_production = Integer.valueOf(action.split(" ")[1]);
                    String pdp = getProductionForReduce(nr_production).getPdp();
                    String psp = getProductionForReduce(nr_production).getPsp();
                    String concat_production = "";
                    while (!work_stack.empty()) {
                        String pop_elem = work_stack.pop();
                        if (pdp.contains(pop_elem)) {
                            concat_production = pop_elem + concat_production;
                        }
                        if (concat_production.equals(pdp)) {
                            String new_state = gotoTable.get(work_stack.peek() + ":" + psp);
                            work_stack.push(psp);
                            work_stack.push(new_state);
                            break;
                        }
                    }
                    System.out.println(work_stack);
                    System.out.println(in_stack);
                    System.out.println("reducere: " + nr_production);
                    System.out.println();
                    bi.add(nr_production);
                    continue;
                }
                if (action.equals("acceptat") && in_stack.size() == 1) {
                    System.out.println("Acceptat");
                    System.out.println(bi);
                    System.out.println(states);
                    return;
                } else {
                    System.out.println("Erroare - stiva de intrare != stiva de lucru");
                    break;
                }
            }catch (Exception e){
                System.out.println("ERROR: goto(" + work_stack.peek() + "," + in_stack.peek() + ") = error");
                break;
            }
        }
    }

    /**
     * @param input
     * @return lista cu toate char urile split-uite, pt a le putea adauga in stack
     */
    public List<String> parseInput(String input) throws Exception {
        Map<Integer, String> vals = new TreeMap<>(Collections.reverseOrder());
        String input_pattern = input;

        for (int i = 0; i < input.length(); i++) {
            for (String symbol : getAllSymbols()) {
                if (input_pattern.contains(symbol)) {
                    vals.put(input_pattern.indexOf(symbol), symbol);
                    input_pattern = StringUtils.replaceOnce(input_pattern, symbol, "*");
                }
            }
        }
        if (StringUtils.countMatches(input_pattern, "*") != input_pattern.length()) {
            throw new Exception("Secventa contine date invalide!");
        }
        List<String> values = new ArrayList<>();
        for (Map.Entry<Integer, String> entry : vals.entrySet()) {
            values.add(entry.getValue());
        }
        return values;
    }

    public Map<String, List<Production>> getCanonic() {
        return canonic;
    }

    public Map<String, List<Production>> getGoto_map() {
        return goto_map;
    }

    public int getStates() {
        return states;
    }

    public Map<String, String> getGotoTable() {
        return gotoTable;
    }

    public Map<String, String> getActionsTable() {
        return actionsTable;
    }

    public List<String> getDisplay_canonic() {
        return display_canonic;
    }

    public static void main(String[] args) throws Exception {
        Grammar grammar = new Grammar("grammar.txt");
        LR0 lr0 = new LR0(grammar);
        lr0.colCan_lr0();
//        System.out.println("andrei".replace("a","FOUND"));
//        NonTerminal nonTerminal = lr0.getNonTerminalBeforeDot(".S");
//        if (nonTerminal == null) {
//            System.out.println("da");
//        }

//        test closure
//        Production p = new Production("S", "D.B");
//        List<Production> l = new ArrayList<>();
//        l.add(p);
//        List<Production> list = lr0.closure(l);
//        for (Production production : list) {
//            System.out.println(production.getPsp() + " -> " + production.getPdp());
//        }

//        test goto
//        List<Production> states = new ArrayList<>();
//        states.add(new Production("S", "D.B"));
//        states.add(new Production("B", ".b"));
//        states.add(new Production("B", ".c"));
//        List<Production> list = lr0.gotof(states, "B");
//        for (Production production : list) {
//            System.out.println(production.getPsp() + " -> " + production.getPdp());
//        }

        //colectia canonica
//        Map<String, List<Production>> map = new TreeMap<>(lr0.getCanonic());
//        for (Map.Entry<String, List<Production>> entry : map.entrySet()) {
//            System.out.print(entry.getKey() + ": ");
//            for (Production production : entry.getValue()) {
//                System.out.print(production.getPsp() + " -> " + production.getPdp() + " ");
//            }
//            System.out.println();
//        }
//        System.out.println("***************************");
//        //goto
//        Map<String, List<Production>> map2 = new TreeMap<>(lr0.getGoto_map());
//        for (Map.Entry<String, List<Production>> entry : map2.entrySet()) {
//            System.out.print(entry.getKey() + ": ");
//            for (Production production : entry.getValue()) {
//                System.out.print(production.getPsp() + " -> " + production.getPdp() + " ");
//            }
//            System.out.println();
//        }

        //table goto
//        lr0.table();
//        Map<String, String> map2 = new TreeMap<>(lr0.getGotoTable());
//        for (Map.Entry<String, String> entry : map2.entrySet()) {
//            System.out.print(entry.getKey() + ": ");
//            System.out.print(entry.getValue());
//            System.out.println();
//        }

        //actions
//        lr0.table();
//        Map<String, String> map2 = new TreeMap<>(lr0.getActionsTable());
//        for (Map.Entry<String, String> entry : map2.entrySet()) {
//            System.out.print(entry.getKey() + ": ");
//            System.out.print(entry.getValue());
//            System.out.println();
//        }
//        lr0.table();
//        lr0.analyze("abdc");

        String a = "paulp+";
        a = StringUtils.replaceOnce(a, "+", "c");
        System.out.println(a);
    }
}
