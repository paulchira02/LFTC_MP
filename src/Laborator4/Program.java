package Laborator4;

import java.util.*;

public class Program {
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_RESET = "\u001B[0m";

    public static Map<Integer, String> mainMenu() {
        Map<Integer, String> options = new HashMap<>();
        options.put(1, "Grammar");
        options.put(2, "LR(0)");
        options.put(0, "Exit");
        return options;
    }

    public static Map<Integer, String> grammarSubMenu() {
        Map<Integer, String> options = new HashMap<>();
        options.put(1, "Set of terminals");
        options.put(2, "Set of non-terminals");
        options.put(3, "Set of productions");
        options.put(4, "The productions of a given non-terminal symbol");
        options.put(5, "Is regular grammar?");
        options.put(0, "Exit");
        return options;
    }

    public static Map<Integer, String> lr0SubMenu() {
        Map<Integer, String> options = new HashMap<>();
        options.put(1, "Canonic collection states");
        options.put(2, "LR(0) table");
        options.put(3, "Verify the sequence");
        options.put(0, "Exit");
        return options;
    }

    public static Map<Integer, Runnable> runGrammarOptions(Grammar grammar) {
        Map<Integer, Runnable> runOptions = new HashMap<>();
        runOptions.put(1, () -> {
            System.out.print("N = { " + grammar.getN().get(0));
            for (int i = 1; i < grammar.getN().size(); i++) {
                System.out.print(", " + grammar.getN().get(i));
            }
            System.out.println(" }");
        });
        runOptions.put(2, () -> {
            System.out.print("Sigma = { " + grammar.getSigma().get(0));
            for (int i = 1; i < grammar.getSigma().size(); i++) {
                System.out.print(", " + grammar.getSigma().get(i));
            }
            System.out.println(" }");
        });
        runOptions.put(3, () -> {
            for (Production production : grammar.getP()) {
                System.out.println(production.getPsp() + " -> " + production.getPdp());
            }
        });
        runOptions.put(4, () -> {
            Scanner in = new Scanner(System.in);
            try {
                System.out.print("Type a non-terminal: ");
                String nonterminal = in.nextLine();
                System.out.println();
                for (Production production : grammar.getProdOfNonTerminal(new NonTerminal(nonterminal))) {
                    System.out.println(production.getPsp() + " -> " + production.getPdp());
                }
            } catch (Exception e) {
                System.out.println("Be more carrefuly!!");
            }
        });
        runOptions.put(5, () -> {
            if (grammar.isRegularGrammar()) {
                System.out.println("Is regular grammar !!!");
            } else {
                System.out.println("Is not regular grammar !!!");
            }
        });
        return runOptions;
    }

    public static Map<Integer, Runnable> runLROptions(LR0 lr0) {
        Map<Integer, Runnable> runOptions = new HashMap<>();
        runOptions.put(1, () -> {
//            Map<String, List<Production>> map = new TreeMap<>(lr0.getCanonic());
//            for (Map.Entry<String, List<Production>> entry : map.entrySet()) {
//                System.out.print(entry.getKey() + ": ");
//                for (Production production : entry.getValue()) {
//                    System.out.print(production.getPsp() + " -> " + production.getPdp() + " ");
//                }
//                System.out.println();
            for(String s: lr0.getDisplay_canonic()){
                System.out.println(s);
            }
        });
        runOptions.put(2, () -> {
            System.out.println(ANSI_YELLOW + "GOTO RELATIONS" + ANSI_RESET);
            Map<String, String> table = new TreeMap<>(lr0.getGotoTable());
            for (Map.Entry<String, String> entry : table.entrySet()) {
                System.out.print(entry.getKey() + ": ");
                System.out.print(entry.getValue());
                System.out.println();
            }
            System.out.println(ANSI_YELLOW + "ACTIONS RELATIONS" + ANSI_RESET);
            Map<String, String> actions = new TreeMap<>(lr0.getActionsTable());
            for (Map.Entry<String, String> entry : actions.entrySet()) {
                System.out.print(entry.getKey() + ": ");
                System.out.print(entry.getValue());
                System.out.println();
            }
        });
        runOptions.put(3, () -> {
            Scanner in = new Scanner(System.in);
            System.out.println("Type the sequence:");
            String sequence= in.nextLine();
            try {
                lr0.analyze(sequence);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return runOptions;
    }


    public static void displayMenu(Map<Integer, String> options) {
        for (Map.Entry<Integer, String> entry : options.entrySet()) {
            System.out.println(entry.getKey() + ". " + entry.getValue());
        }
    }

    public static void main(String[] args) {
        Grammar grammar = new Grammar("grammar.txt");
        LR0 lr0 = new LR0(grammar);
        lr0.colCan_lr0();
        lr0.table();

        boolean showMenu = true;
        Scanner in = new Scanner(System.in);
        int option;
        int suboption;
        displayMenu(mainMenu());
        while (showMenu) {
            try {
                System.out.print("Type the option: ");
                option = in.nextInt();
                System.out.println();
                while (option == 1) {
                    displayMenu(grammarSubMenu());
                    try {
                        System.out.print("Type the option: ");
                        suboption = in.nextInt();
                        System.out.println();
                        if (suboption == 0) {
                            displayMenu(mainMenu());
                            break;
                        } else {
                            runGrammarOptions(grammar).get(suboption).run();
                        }
                    } catch (Exception e) {
                        System.out.println("Introduceti doar numerele disponibile!!");
                    }
                }
                while (option == 2) {
                    displayMenu(lr0SubMenu());
                    try {
                        System.out.print("Type the option: ");
                        suboption = in.nextInt();
                        System.out.println();
                        if (suboption == 0) {
                            displayMenu(mainMenu());
                            break;
                        } else {
                            runLROptions(lr0).get(suboption).run();
                        }
                    } catch (Exception e) {
                        System.out.println("Introduceti doar numerele disponibile!!");
                    }
                }
                if (option == 0) {
                    showMenu = false;
                }
            } catch (Exception e) {
                System.out.println("Introduceti doar numerele disponibile!!");
            }
        }
    }
}
