package Laborator4;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Grammar {
    private List<NonTerminal> N;
    private List<Terminal> Sigma;
    private List<Production> P;
    private String Start;

    public Grammar(String filename) {
        this.N = new ArrayList<>();
        this.Sigma = new ArrayList<>();
        this.P = new ArrayList<>();
        this.Start = "";
        this.readGrammarFromFile(filename);
    }

    public Grammar(List<NonTerminal> n, List<Terminal> sigma, List<Production> p, String start) {
        this.N = n;
        this.Sigma = sigma;
        this.P = p;
        this.Start = start;
    }

    private void readGrammarFromFile(String filename) {

        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(filename));
            //read nonterminals from first line of a file
            String[] N = in.readLine().split(" ");
            for (String nt : N) {
                NonTerminal nonTerminal = new NonTerminal(nt);
                this.N.add(nonTerminal);
            }
            //read nonterminals from 2nd line of a file
            String[] Sigma = in.readLine().split(" ");
            for (String t : Sigma) {
                Terminal terminal = new Terminal(t);
                this.Sigma.add(terminal);
            }
            //read start symbol from 3rd line from a file
            this.Start = in.readLine();
            //read productions
            String p = "";
            while ((p = in.readLine()) != null) {
                String[] prod = p.split(" ");
                Production production = new Production();
                production.setPsp(prod[0]);
                String pdp = prod[1];
                for (int i = 2; i < prod.length; i++) {
                    pdp = pdp + " | " + prod[i];
                }
                production.setPdp(pdp);
                this.P.add(production);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Production> getProdOfNonTerminal(NonTerminal nonTerminal) {
        List<Production> list = new ArrayList<>();
        for (Production production : this.P) {
            if (production.getPsp().equals(nonTerminal.getValue())) {
                list.add(production);
            }
        }
        return list;
    }

    public boolean sigmaContainsTerminal(List<Terminal> Sigma, Terminal terminal) {
        for (Terminal t : Sigma) {
            if (t.getValue().equals(terminal.getValue())) {
                return true;
            }
        }
        return false;
    }

    public boolean NContainsNonTerminal(List<NonTerminal> N, NonTerminal nonTerminal) {
        for (NonTerminal nt : N) {
            if (nt.getValue().equals(nonTerminal.getValue())) {
                return true;
            }
        }
        return false;
    }

    public boolean isRegularGrammar() {
        boolean isRegular = true;
        int lineNumber = 0;
        for (Production production : this.P) {
            if (production.getPdp().length() > 1) {
                String[] atoms = production.getPdp().split("\\|");
                for (String atom : atoms) {
                    atom = atom.trim();
                    if (atom.length() > 2) {
                        isRegular = false;
                        return isRegular;
                    }
                    if (atom.length() == 2) {
                        String symbol1 = atom.substring(0, 1);
                        String symbol2 = atom.substring(1, 2);
                        boolean checkNext = true;
                        //case aA
                        if ((sigmaContainsTerminal(Sigma, new Terminal(symbol1)) == false) || (NContainsNonTerminal(N, new NonTerminal(symbol2)) == false) || (Start.equals(symbol2) == true)) {
//                        if ((sigmaContainsTerminal(Sigma, new Terminal(symbol1)) == false) || (NContainsNonTerminal(N, new NonTerminal(symbol2)) == false)) {
                            isRegular = false;
                            return isRegular;
                        } else {
                            checkNext = false;
                        }
                        //case Aa
                        if (checkNext && ((NContainsNonTerminal(N, new NonTerminal(symbol1)) == false) || (sigmaContainsTerminal(Sigma, new Terminal(symbol2)) == false) || (Start.equals(symbol1) == true))) {
//                        if (checkNext && ((NContainsNonTerminal(N, new NonTerminal(symbol1)) == false) || (sigmaContainsTerminal(Sigma, new Terminal(symbol2)) == false))) {
                            isRegular = false;
                            return isRegular;
                        }
                    }
                    if (atom.length() == 1) {
                        boolean checkNext = true;
                        if (atom.equals("E")) {
                            if (production.getPdp().contains("E") && lineNumber != 0) {
                                isRegular = false;
                                return isRegular;
                            } else {
                                checkNext = false;
                            }
                        }
                        if (checkNext && (sigmaContainsTerminal(Sigma, new Terminal(atom)) == false)) {
                            isRegular = false;
                            return isRegular;
                        }
                    }
                }
            } else {
                String atom = production.getPdp();
                if ((sigmaContainsTerminal(Sigma, new Terminal(atom)) == false))
                    isRegular = false;
                return isRegular;
            }
            lineNumber++;
        }
        return isRegular;
    }

    public List<NonTerminal> getN() {
        return N;
    }

    public void setN(List<NonTerminal> n) {
        N = n;
    }

    public List<Terminal> getSigma() {
        return Sigma;
    }

    public void setSigma(List<Terminal> sigma) {
        Sigma = sigma;
    }

    public List<Production> getP() {
        return P;
    }

    public void setP(List<Production> p) {
        P = p;
    }

    public String getStart() {
        return Start;
    }

    public void setStart(String start) {
        Start = start;
    }
}
