package Laborator4;

public class Transition {
    private NonTerminal nonTerminal;
    private Terminal terminal;
    private NonTerminal result;

    public Transition() {
    }

    public Transition(NonTerminal nonTerminal, Terminal terminal, NonTerminal result) {
        this.nonTerminal = nonTerminal;
        this.terminal = terminal;
        this.result = result;
    }

    public NonTerminal getNonTerminal() {
        return nonTerminal;
    }

    public void setNonTerminal(NonTerminal nonTerminal) {
        this.nonTerminal = nonTerminal;
    }

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public NonTerminal getResult() {
        return result;
    }

    public void setResult(NonTerminal result) {
        this.result = result;
    }
}
